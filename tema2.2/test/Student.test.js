let assert = require('assert')

describe('Student', function(){
	describe('#isPassing', function() {
		it('should return false', function(){
			let Student = require('../app').Student
			let Grade = require('../app').Grade
			let student = new Student('jim')
			student.addGrade(new Grade('CS101', 4))
			student.addGrade(new Grade('CS102', 6))
			student.addGrade(new Grade('CS103', 7))
			assert.equal(student.isPassing(), false)
		})
		it('should return true', function(){
			let Student = require('../app').Student
			let Grade = require('../app').Grade
			let student = new Student('john')
			student.addGrade(new Grade('CS101', 9))
			student.addGrade(new Grade('CS102', 6))
			student.addGrade(new Grade('CS103', 7))
			assert.equal(student.isPassing(), true)
		})
	})
	describe('#getPassingGrades', function() {
		it('should return passing grades as an array', function(){
			let Student = require('../app').Student
			let Grade = require('../app').Grade
			let student = new Student('jim')
			student.addGrade(new Grade('CS101', 4))
			student.addGrade(new Grade('CS102', 6))
			student.addGrade(new Grade('CS103', 7))
			assert.deepEqual(student.getPassingGrades(), [6, 7])
		})
	})
	describe('#getFailedSubjects', function() {
		it('should return failed subjects as an array', function(){
			let Student = require('../app').Student
			let Grade = require('../app').Grade
			let student = new Student('jim')
			student.addGrade(new Grade('CS101', 4))
			student.addGrade(new Grade('CS102', 6))
			student.addGrade(new Grade('CS103', 7))
			assert.deepEqual(student.getFailedSubjects(), ['CS101'])
		})
	})

})


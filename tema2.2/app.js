class Person{
	constructor(name){
		this.name = name
	}
}

class Student extends Person{
	constructor(name, faculty, year){
		super(name)
		this.faculty = faculty
		this.year = year
		this.grades = []
	}
	addGrade( g){
		this.grades.push(new Grade(g.subject,g.value))
	}
	isPassing(){
		for(let i=0;i<this.grades.length;i++)
		{
			if(!this.grades[i].isPassingGrade())
			return false
			
		}
		return true
	}
	getPassingGrades(){
		let newGrades=[]
		for(let i=0;i<this.grades.length;i++)
		{
			if(this.grades[i].isPassingGrade())
newGrades.push(this.grades[i].value)
		}
		return newGrades
	}
	getFailedSubjects(){
		let newSubjects=[]
		for(let i=0;i<this.grades.length;i++)
		{
			if(this.grades[i].value<5)
newSubjects.push(this.grades[i].subject)
		}
		return newSubjects
	}
}

class Grade{
	constructor(subject, value){
		this.subject = subject
		this.value = value
	}
	isPassingGrade(){
	if(this.value>=5)
	{
		return true
	}
	else{
		return false
		
	}
	}
}

module.exports.Student = Student
module.exports.Grade = Grade
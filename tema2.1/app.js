const dictionary = ['javascript', 'java', 'python']

/*
functia sanitize primeste ca parametru un text si un dictionar
cuvintele din dictionar sunt inlocuite in text cu prima litera urmata de o serie de asteriscuri (egala cu numarul de litere inlocuite) urmata de ultima litera
e.g. daca 'decembrie' exista in dictionar, va fi inlocuit cu 'd*******e'
*/
function sanitize(text, dictionary){
    for(var i=0; i<dictionary.length; i++){
        if(text.includes(dictionary[i])){
            var first=dictionary[i].substring(0,1)
            var wordLength= dictionary[i].length
            var last=dictionary[i].substring(wordLength-1, wordLength)
            var newWord=first + '*'.repeat(wordLength-2)+last
        }
        
        text= text.replace(dictionary[i], newWord)

    }
    return text;
 
}


module.exports.dictionary = dictionary
module.exports.sanitize = sanitize